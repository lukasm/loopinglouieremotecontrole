(function ($, Gauge) {
    'use strict';
    var tacho;
    var socket;
    $(document).ready(function () {
        socket = io("http://192.168.2.104");
        
        tacho = new Gauge("tachometer", {'mode': 'needle', 'range': {'min': 0, 'max': 100 } });
        tacho.draw(0);
        $('#slider').change(function () {
            var val = $('#slider').val();
            tacho.draw(val);
            socket.emit('speedChange',val);
        });
        socket.on('setSpeed', function(speed){
            $('#slider').val(speed);
            tacho.draw(speed);
        });
    });
}(jQuery, Gauge));