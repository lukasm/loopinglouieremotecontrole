var exec = require('child_process').exec;
var connections = 0,
    currSpeed = 0;
var connect = require('connect');
var serveStatic = require('serve-static');
var server = connect().use(serveStatic("public")).listen(80);

var io = require('socket.io')(server);

exec("gpio mode 1 pwm");



/**
 * Wird aufgerufen wenn sich ein client verbindet
 * @param {Object} socket Client der sich verbunden hat
 */
io.on('connection', function (socket) {
    connections = connections + 1;
    console.log("Connections:" + connections - 1);
    io.emit("setSpeed", currSpeed);
    /**
     * Wird aufgerufen wen sich die Geschwindigkeit auf einen client ändert
     * @param {[[Type]]} speed [[Description]]
     */
    socket.on('speedChange', function (speed) {
        console.log("Speedchange:" + speed);
        if (speed > -1 && speed < 101) {
            currSpeed = speed;
            exec("gpio pwm 1 " + Math.round((1023 / 100) * speed));
            console.log("PWM" + Math.round((1023 / 100) * speed));
            socket.broadcast.emit("setSpeed", speed);
        }
    });
});